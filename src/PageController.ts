/**
 * Created by DreamsUltra10 on 04.12.2016.
 */
import {PagesEnum} from "./enum/PagesEnum";
import {AbstractPage} from "./pages/AbstractPage";
import {CreateBannerPage} from "./pages/CreateBannerPage";
import {ChooseBannerPage} from "./pages/ChooseBannerPage";


export class PageController
{
    private pages:{[key:string]:any};
    private currentPage:AbstractPage;

    constructor()
    {

    }

    //--------------------------------------------------------------------------
    //							PUBLIC METHODS
    //--------------------------------------------------------------------------
    initialize():void
    {
        this.initPages();
        this.changePage();
    }

    // Equal to change state in current context
    changePage(pageName:string = PagesEnum.CHOOSE_BANNER):void
    {
        if(this.currentPage != null)
        {
            this.currentPage.remove();
        }

        this.currentPage = this.pages[pageName];
        this.currentPage.add();
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    private initPages():void
    {
        this.pages = {};
        this.pages[PagesEnum.CREATE_BANNER] = new CreateBannerPage(this);
        this.pages[PagesEnum.CHOOSE_BANNER] = new ChooseBannerPage(this);
    }
}