(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    /**
     * Created by DreamsUltra10 on 05.12.2016.
     */
    var Config = (function () {
        function Config() {
        }
        Config.BANNERS_PATH = "banners/";
        return Config;
    }());
    exports.Config = Config;
});
//# sourceMappingURL=Config.js.map