(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    var PagesEnum = (function () {
        function PagesEnum() {
        }
        PagesEnum.CHOOSE_BANNER = "CHOOSE_BANNER";
        PagesEnum.CREATE_BANNER = "CREATE_BANNER";
        return PagesEnum;
    }());
    exports.PagesEnum = PagesEnum;
});
//# sourceMappingURL=PagesEnum.js.map