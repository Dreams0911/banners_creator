import {AbstractPage} from "./AbstractPage";
import {RequestsEnum} from "../enum/RequestsEnum";
import {PagesEnum} from "../enum/PagesEnum";
import {PageController} from "../PageController";
/**
 * Created by DreamsUltra10 on 04.12.2016.
 */

export class CreateBannerPage extends AbstractPage
{
    constructor(pageController:PageController)
    {
        super(pageController);

        this.container = document.createElement("div");
        this.container.id = "page_container";
        this.container.className = "page_container";
        this.init();
    }

    //--------------------------------------------------------------------------
    //							PUBLIC METHODS
    //--------------------------------------------------------------------------
    add():void
    {
        document.body.appendChild(this.container);
    }

    remove():void
    {
        document.body.removeChild(this.container);
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    private init(): void
    {
        let div:HTMLElement = document.createElement('div');
        div.className = 'well';

        div.innerHTML =
            "<h4><strong> Create new banner </strong></h4>" +
            "<div class='form-group'>  " +
                " <input class='form-control' type='text' id='banner_name' value='banner'/> " +
            "</div>" +

            "<br><h4>New category</h4>" +
            "<div class='form-group'> " +
                "<label for='new_short_name_1'>Short name 1 </label>" +
                " <input type='text' class='form-control' id='new_short_name_1'/> " +
                "<input type='file' id='new_icon_1'/> " +
            "</div>" +
            "<div class='form-group'> " +
                "<label for='new_short_name_2'>Short name 2 </label>" +
                " <input type='text' class='form-control' id='new_short_name_2'/> " +
                " <input type='file' id='new_icon_2'/> " +
            "</div>" +
            "<div class='form-group'> " +
                "<label for='new_short_name_3'>Short name 3 </label>" +
                " <input type='text' class='form-control' id='new_short_name_3'/> " +
                " <input type='file' id='new_icon_3'/> " +
            "</div>" +

            "<br/>" +

            "<br><h4 class='form-group'>Popular category</h4>" +
            "<div class='form-group'> " +
                "<label for='popular_short_name_1'>Short name 1 </label>" +
                " <input type='text' class='form-control' id='popular_short_name_1'/> " +
                " <input type='file' id='popular_icon_1'/> " +
            "</div>" +
            "<div class='form-group'> " +
                "<label for='popular_short_name_2'>Short name 2 </label>" +
                " <input type='text' class='form-control' id='popular_short_name_2'/> " +
                " <input type='file' id='popular_icon_2'/> " +
            "</div>" +
            "<div class='form-group'> " +
                "<label for='popular_short_name_3'>Short name 3 </label>" +
                " <input type='text' class='form-control' id='popular_short_name_3'/> " +
                " <input type='file' id='popular_icon_3'/> " +
            "</div>";

        let saveBtn:HTMLElement = document.createElement('button');
        saveBtn.id = "save_btn";
        saveBtn.className = "btn btn-default";
        saveBtn.innerHTML = "SAVE";
        saveBtn.onclick = this.saveClickHandler.bind(this);
        div.appendChild(saveBtn);

        this.container.appendChild(div);
    }

    private generateFormData():FormData
    {
        let fd:FormData = new FormData();
        fd.append("banner_name", (<HTMLInputElement>document.getElementById("banner_name")).value);

        // NEW
        fd.append("new_short_name_1", (<HTMLInputElement>document.getElementById("new_short_name_1")).value);
        fd.append("new_short_name_2", (<HTMLInputElement>document.getElementById("new_short_name_2")).value);
        fd.append("new_short_name_3", (<HTMLInputElement>document.getElementById("new_short_name_3")).value);
        fd.append("new_icon_1", (<HTMLInputElement>document.getElementById("new_icon_1")).files[0]);
        fd.append("new_icon_2", (<HTMLInputElement>document.getElementById("new_icon_2")).files[0]);
        fd.append("new_icon_3", (<HTMLInputElement>document.getElementById("new_icon_3")).files[0]);

        // POPULAR
        fd.append("popular_short_name_1", (<HTMLInputElement>document.getElementById("popular_short_name_1")).value);
        fd.append("popular_short_name_2", (<HTMLInputElement>document.getElementById("popular_short_name_2")).value);
        fd.append("popular_short_name_3", (<HTMLInputElement>document.getElementById("popular_short_name_3")).value);
        fd.append("popular_icon_1", (<HTMLInputElement>document.getElementById("popular_icon_1")).files[0]);
        fd.append("popular_icon_2", (<HTMLInputElement>document.getElementById("popular_icon_2")).files[0]);
        fd.append("popular_icon_3", (<HTMLInputElement>document.getElementById("popular_icon_3")).files[0]);

        return fd;
    }

    private generateAjaxSettings():JQueryAjaxSettings
    {
        let fd:FormData = this.generateFormData();

        let settings:JQueryAjaxSettings = {};
        settings.url = RequestsEnum.CREATE_BANNER;
        settings.data = fd;
        settings.processData = false; // Setting processData to false lets you prevent jQuery from automatically transforming the data into a query string.
        settings.contentType = false;
        settings.success = this.bannerCreateSuccessHandler.bind(this);
        settings.error = this.bannerCreateErrorHandler.bind(this);

        return settings;
    }

    //--------------------------------------------------------------------------
    //								HANDLERS
    //--------------------------------------------------------------------------
    private saveClickHandler():void
    {
        let settings:JQueryAjaxSettings = this.generateAjaxSettings();
        $.post(settings);
    }

    // BANNER CREATE
    private bannerCreateSuccessHandler(data:any):void
    {
        console.log("bannerCreateSuccessHandler");
        this.pageController.changePage(PagesEnum.CHOOSE_BANNER);
    }
    private bannerCreateErrorHandler(jqXHR:JQueryXHR):void
    {
        console.log("bannerCreateErrorHandler");
    }
}