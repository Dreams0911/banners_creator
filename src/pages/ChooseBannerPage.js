var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "./AbstractPage", "../enum/RequestsEnum", "../enum/PagesEnum", "../Config"], factory);
    }
})(function (require, exports) {
    "use strict";
    var AbstractPage_1 = require("./AbstractPage");
    var RequestsEnum_1 = require("../enum/RequestsEnum");
    var PagesEnum_1 = require("../enum/PagesEnum");
    var Config_1 = require("../Config");
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    var ChooseBannerPage = (function (_super) {
        __extends(ChooseBannerPage, _super);
        function ChooseBannerPage(pageController) {
            _super.call(this, pageController);
            this.setPageURL();
        }
        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        ChooseBannerPage.prototype.add = function () {
            this.container = document.createElement("div");
            this.container.id = "page_container";
            this.container.className = "page_container";
            document.body.appendChild(this.container);
            this.getBannersList();
        };
        ChooseBannerPage.prototype.remove = function () {
            document.body.removeChild(this.container);
            this.container = null;
            this.bannersList = null;
            this.bannersSelector = null;
            this.bannerUrl = null;
        };
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        ChooseBannerPage.prototype.init = function () {
            var htmlString = "<div class='well'>" +
                "<div class='form-group'>" +
                "<label for='banners_selector'>Banner</label>" +
                "<select class='form-control' id='banners_selector'>";
            for (var i = 0; i < this.bannersList.length; i++) {
                htmlString += "<option class='form-control' value='" + this.bannersList[i] + "'> " + this.bannersList[i] + " </option>";
            }
            htmlString +=
                "</select>" +
                    "</div>" +
                    "<div class='form-group'>" +
                    "<label for='banner_url'>URL</label>" +
                    "<input class='form-control' type='text' id='banner_url' value='' readonly size='100'/> " +
                    "</div>" +
                    "<div class='form-group'>" +
                    "<button class='btn btn-default' id='delete_btn'> Delete </button> " +
                    "<button class='btn btn-default' id='create_new_btn'> Create New </button>" +
                    "</div>" +
                    "</div>";
            this.container.innerHTML = htmlString;
            this.initButtons();
        };
        ChooseBannerPage.prototype.initButtons = function () {
            var createNewBtn = document.getElementById('create_new_btn');
            createNewBtn.onclick = this.createNewClickHandler.bind(this);
            var deleteBtn = document.getElementById('delete_btn');
            deleteBtn.onclick = this.deleteClickHandler.bind(this);
            this.bannersSelector = document.getElementById('banners_selector');
            this.bannersSelector.onchange = this.selectBannerHandler.bind(this);
            this.bannerUrl = document.getElementById('banner_url');
            this.selectBannerHandler();
        };
        ChooseBannerPage.prototype.getBannersList = function () {
            var settings = {};
            settings.url = RequestsEnum_1.RequestsEnum.GET_BANNERS_LIST;
            settings.success = this.getBannersListSuccessHandler.bind(this);
            settings.error = this.getBannersListErrorHandler.bind(this);
            $.post(settings);
        };
        ChooseBannerPage.prototype.setPageURL = function () {
            var url = window.location.href;
            var lastSlashIndex = url.lastIndexOf("/");
            if ((url.length - 1 > lastSlashIndex)) {
                // if opened non root page, index.html for instance
                url = url.slice(0, lastSlashIndex + 1);
            }
            this.pageURL = url;
        };
        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        // GET BANNERS LIST
        ChooseBannerPage.prototype.getBannersListSuccessHandler = function (bannersList) {
            console.log("getBannersListSuccessHandler");
            console.log(bannersList);
            this.bannersList = bannersList;
            this.init();
        };
        ChooseBannerPage.prototype.getBannersListErrorHandler = function (jqXHR) {
            console.log("getBannersListErrorHandler");
        };
        ChooseBannerPage.prototype.selectBannerHandler = function (ev) {
            if (ev === void 0) { ev = null; }
            this.bannerUrl.value = this.pageURL + Config_1.Config.BANNERS_PATH + this.bannersSelector.value;
        };
        ChooseBannerPage.prototype.createNewClickHandler = function () {
            console.log("createNewClickHandler");
            this.pageController.changePage(PagesEnum_1.PagesEnum.CREATE_BANNER);
        };
        ChooseBannerPage.prototype.deleteClickHandler = function () {
            console.log("deleteClickHandler");
            //
            var fd = new FormData();
            fd.append("banner_name", this.bannersSelector.value);
            //
            var settings = {};
            settings.url = RequestsEnum_1.RequestsEnum.DELETE_BANNER;
            settings.data = fd;
            settings.processData = false; // Setting processData to false lets you prevent jQuery from automatically transforming the data into a query string.
            settings.contentType = false;
            settings.success = this.deleteBannerSuccessHandler.bind(this);
            settings.error = this.deleteBannerErrorHandler.bind(this);
            $.post(settings);
        };
        ChooseBannerPage.prototype.deleteBannerSuccessHandler = function (data) {
            console.log("deleteBannerSuccessHandler");
            this.remove();
            this.add();
        };
        ChooseBannerPage.prototype.deleteBannerErrorHandler = function (jqXHR) {
            console.log("deleteBannerErrorHandler");
        };
        return ChooseBannerPage;
    }(AbstractPage_1.AbstractPage));
    exports.ChooseBannerPage = ChooseBannerPage;
});
//# sourceMappingURL=ChooseBannerPage.js.map