import {AbstractPage} from "./AbstractPage";
import {RequestsEnum} from "../enum/RequestsEnum";
import {PagesEnum} from "../enum/PagesEnum";
import {isNullOrUndefined} from "util";
import {PageController} from "../PageController";
import {Config} from "../Config";
/**
 * Created by DreamsUltra10 on 04.12.2016.
 */

export class ChooseBannerPage extends AbstractPage
{
    private pageURL:string;

    private bannersList:Array<string>;
    private bannersSelector:HTMLInputElement;
    private bannerUrl:HTMLInputElement;

    constructor(pageController:PageController)
    {
        super(pageController);
        this.setPageURL();
    }

    //--------------------------------------------------------------------------
    //							PUBLIC METHODS
    //--------------------------------------------------------------------------
    add():void
    {
        this.container = document.createElement("div");
        this.container.id = "page_container";
        this.container.className = "page_container";
        document.body.appendChild(this.container);
        this.getBannersList();
    }

    remove():void
    {
        document.body.removeChild(this.container);
        this.container = null;

        this.bannersList = null;
        this.bannersSelector = null;
        this.bannerUrl = null;
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    private init(): void
    {
        let htmlString:string =
            "<div class='well'>" +
                "<div class='form-group'>" +
                    "<label for='banners_selector'>Banner</label>" +
                    "<select class='form-control' id='banners_selector'>";

        for(let i:number=0; i<this.bannersList.length; i++)
        {
            htmlString += "<option class='form-control' value='" + this.bannersList[i] + "'> " + this.bannersList[i] + " </option>"
        }

        htmlString +=
                    "</select>" +
                "</div>" +

                "<div class='form-group'>" +
                    "<label for='banner_url'>URL</label>" +
                    "<input class='form-control' type='text' id='banner_url' value='' readonly size='100'/> " +
                "</div>" +

                "<div class='form-group'>" +
                    "<button class='btn btn-default' id='delete_btn'> Delete </button> " +
                    "<button class='btn btn-default' id='create_new_btn'> Create New </button>" +
                "</div>" +
            "</div>";

        this.container.innerHTML = htmlString;
        this.initButtons();
    }

    private initButtons():void
    {
        let createNewBtn:HTMLElement = document.getElementById('create_new_btn');
        createNewBtn.onclick = this.createNewClickHandler.bind(this);

        let deleteBtn:HTMLElement = document.getElementById('delete_btn');
        deleteBtn.onclick = this.deleteClickHandler.bind(this);

        this.bannersSelector = <HTMLInputElement>document.getElementById('banners_selector');
        this.bannersSelector.onchange = this.selectBannerHandler.bind(this);

        this.bannerUrl = <HTMLInputElement>document.getElementById('banner_url');
        this.selectBannerHandler();
    }

    private getBannersList():void
    {
        let settings:JQueryAjaxSettings = {};
        settings.url = RequestsEnum.GET_BANNERS_LIST;
        settings.success = this.getBannersListSuccessHandler.bind(this);
        settings.error = this.getBannersListErrorHandler.bind(this);
        $.post(settings);
    }

    private setPageURL():void
    {
        let url:string = window.location.href;
        let lastSlashIndex:number = url.lastIndexOf("/");
        if((url.length-1 > lastSlashIndex))
        {
            // if opened non root page, index.html for instance
            url = url.slice(0, lastSlashIndex+1);
        }

        this.pageURL = url;
    }

    //--------------------------------------------------------------------------
    //								HANDLERS
    //--------------------------------------------------------------------------
    // GET BANNERS LIST
    private getBannersListSuccessHandler(bannersList:Array<string>):void
    {
        console.log("getBannersListSuccessHandler");
        console.log(bannersList);

        this.bannersList = bannersList;
        this.init();
    }
    private getBannersListErrorHandler(jqXHR:JQueryXHR):void
    {
        console.log("getBannersListErrorHandler");
    }

    private selectBannerHandler(ev:UIEvent = null):void
    {
        this.bannerUrl.value = this.pageURL + Config.BANNERS_PATH + this.bannersSelector.value;
    }

    private createNewClickHandler():void
    {
        console.log("createNewClickHandler");
        this.pageController.changePage(PagesEnum.CREATE_BANNER);
    }

    private deleteClickHandler():void
    {
        console.log("deleteClickHandler");

        //
        let fd:FormData = new FormData();
        fd.append("banner_name", this.bannersSelector.value);

        //
        let settings:JQueryAjaxSettings = {};
        settings.url = RequestsEnum.DELETE_BANNER;
        settings.data = fd;
        settings.processData = false; // Setting processData to false lets you prevent jQuery from automatically transforming the data into a query string.
        settings.contentType = false;
        settings.success = this.deleteBannerSuccessHandler.bind(this);
        settings.error = this.deleteBannerErrorHandler.bind(this);
        $.post(settings);
    }

    private deleteBannerSuccessHandler(data:any):void
    {
        console.log("deleteBannerSuccessHandler");

        this.remove();
        this.add();
    }

    private deleteBannerErrorHandler(jqXHR:JQueryXHR):void
    {
        console.log("deleteBannerErrorHandler");
    }
}