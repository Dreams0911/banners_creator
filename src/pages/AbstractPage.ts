import {PageController} from "../PageController";
/**
 * Created by DreamsUltra10 on 04.12.2016.
 */

export abstract class AbstractPage
{
    protected container:HTMLElement;

    constructor(protected pageController:PageController)
    {
    }

    //--------------------------------------------------------------------------
    //							PUBLIC METHODS
    //--------------------------------------------------------------------------
    abstract add():void;
    abstract remove():void;
}