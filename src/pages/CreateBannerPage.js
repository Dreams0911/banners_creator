var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "./AbstractPage", "../enum/RequestsEnum", "../enum/PagesEnum"], factory);
    }
})(function (require, exports) {
    "use strict";
    var AbstractPage_1 = require("./AbstractPage");
    var RequestsEnum_1 = require("../enum/RequestsEnum");
    var PagesEnum_1 = require("../enum/PagesEnum");
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    var CreateBannerPage = (function (_super) {
        __extends(CreateBannerPage, _super);
        function CreateBannerPage(pageController) {
            _super.call(this, pageController);
            this.container = document.createElement("div");
            this.container.id = "page_container";
            this.container.className = "page_container";
            this.init();
        }
        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        CreateBannerPage.prototype.add = function () {
            document.body.appendChild(this.container);
        };
        CreateBannerPage.prototype.remove = function () {
            document.body.removeChild(this.container);
        };
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        CreateBannerPage.prototype.init = function () {
            var div = document.createElement('div');
            div.className = 'well';
            div.innerHTML =
                "<h4><strong> Create new banner </strong></h4>" +
                    "<div class='form-group'>  " +
                    " <input class='form-control' type='text' id='banner_name' value='banner'/> " +
                    "</div>" +
                    "<br><h4>New category</h4>" +
                    "<div class='form-group'> " +
                    "<label for='new_short_name_1'>Short name 1 </label>" +
                    " <input type='text' class='form-control' id='new_short_name_1'/> " +
                    "<input type='file' id='new_icon_1'/> " +
                    "</div>" +
                    "<div class='form-group'> " +
                    "<label for='new_short_name_2'>Short name 2 </label>" +
                    " <input type='text' class='form-control' id='new_short_name_2'/> " +
                    " <input type='file' id='new_icon_2'/> " +
                    "</div>" +
                    "<div class='form-group'> " +
                    "<label for='new_short_name_3'>Short name 3 </label>" +
                    " <input type='text' class='form-control' id='new_short_name_3'/> " +
                    " <input type='file' id='new_icon_3'/> " +
                    "</div>" +
                    "<br/>" +
                    "<br><h4 class='form-group'>Popular category</h4>" +
                    "<div class='form-group'> " +
                    "<label for='popular_short_name_1'>Short name 1 </label>" +
                    " <input type='text' class='form-control' id='popular_short_name_1'/> " +
                    " <input type='file' id='popular_icon_1'/> " +
                    "</div>" +
                    "<div class='form-group'> " +
                    "<label for='popular_short_name_2'>Short name 2 </label>" +
                    " <input type='text' class='form-control' id='popular_short_name_2'/> " +
                    " <input type='file' id='popular_icon_2'/> " +
                    "</div>" +
                    "<div class='form-group'> " +
                    "<label for='popular_short_name_3'>Short name 3 </label>" +
                    " <input type='text' class='form-control' id='popular_short_name_3'/> " +
                    " <input type='file' id='popular_icon_3'/> " +
                    "</div>";
            var saveBtn = document.createElement('button');
            saveBtn.id = "save_btn";
            saveBtn.className = "btn btn-default";
            saveBtn.innerHTML = "SAVE";
            saveBtn.onclick = this.saveClickHandler.bind(this);
            div.appendChild(saveBtn);
            this.container.appendChild(div);
        };
        CreateBannerPage.prototype.generateFormData = function () {
            var fd = new FormData();
            fd.append("banner_name", document.getElementById("banner_name").value);
            // NEW
            fd.append("new_short_name_1", document.getElementById("new_short_name_1").value);
            fd.append("new_short_name_2", document.getElementById("new_short_name_2").value);
            fd.append("new_short_name_3", document.getElementById("new_short_name_3").value);
            fd.append("new_icon_1", document.getElementById("new_icon_1").files[0]);
            fd.append("new_icon_2", document.getElementById("new_icon_2").files[0]);
            fd.append("new_icon_3", document.getElementById("new_icon_3").files[0]);
            // POPULAR
            fd.append("popular_short_name_1", document.getElementById("popular_short_name_1").value);
            fd.append("popular_short_name_2", document.getElementById("popular_short_name_2").value);
            fd.append("popular_short_name_3", document.getElementById("popular_short_name_3").value);
            fd.append("popular_icon_1", document.getElementById("popular_icon_1").files[0]);
            fd.append("popular_icon_2", document.getElementById("popular_icon_2").files[0]);
            fd.append("popular_icon_3", document.getElementById("popular_icon_3").files[0]);
            return fd;
        };
        CreateBannerPage.prototype.generateAjaxSettings = function () {
            var fd = this.generateFormData();
            var settings = {};
            settings.url = RequestsEnum_1.RequestsEnum.CREATE_BANNER;
            settings.data = fd;
            settings.processData = false; // Setting processData to false lets you prevent jQuery from automatically transforming the data into a query string.
            settings.contentType = false;
            settings.success = this.bannerCreateSuccessHandler.bind(this);
            settings.error = this.bannerCreateErrorHandler.bind(this);
            return settings;
        };
        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        CreateBannerPage.prototype.saveClickHandler = function () {
            var settings = this.generateAjaxSettings();
            $.post(settings);
        };
        // BANNER CREATE
        CreateBannerPage.prototype.bannerCreateSuccessHandler = function (data) {
            console.log("bannerCreateSuccessHandler");
            this.pageController.changePage(PagesEnum_1.PagesEnum.CHOOSE_BANNER);
        };
        CreateBannerPage.prototype.bannerCreateErrorHandler = function (jqXHR) {
            console.log("bannerCreateErrorHandler");
        };
        return CreateBannerPage;
    }(AbstractPage_1.AbstractPage));
    exports.CreateBannerPage = CreateBannerPage;
});
//# sourceMappingURL=CreateBannerPage.js.map