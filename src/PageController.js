(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "./enum/PagesEnum", "./pages/CreateBannerPage", "./pages/ChooseBannerPage"], factory);
    }
})(function (require, exports) {
    "use strict";
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    var PagesEnum_1 = require("./enum/PagesEnum");
    var CreateBannerPage_1 = require("./pages/CreateBannerPage");
    var ChooseBannerPage_1 = require("./pages/ChooseBannerPage");
    var PageController = (function () {
        function PageController() {
        }
        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        PageController.prototype.initialize = function () {
            this.initPages();
            this.changePage();
        };
        // Equal to change state in current context
        PageController.prototype.changePage = function (pageName) {
            if (pageName === void 0) { pageName = PagesEnum_1.PagesEnum.CHOOSE_BANNER; }
            if (this.currentPage != null) {
                this.currentPage.remove();
            }
            this.currentPage = this.pages[pageName];
            this.currentPage.add();
        };
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        PageController.prototype.initPages = function () {
            this.pages = {};
            this.pages[PagesEnum_1.PagesEnum.CREATE_BANNER] = new CreateBannerPage_1.CreateBannerPage(this);
            this.pages[PagesEnum_1.PagesEnum.CHOOSE_BANNER] = new ChooseBannerPage_1.ChooseBannerPage(this);
        };
        return PageController;
    }());
    exports.PageController = PageController;
});
//# sourceMappingURL=PageController.js.map