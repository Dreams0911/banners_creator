(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "./src/PageController"], factory);
    }
})(function (require, exports) {
    "use strict";
    var PageController_1 = require("./src/PageController");
    var pageController = new PageController_1.PageController();
    pageController.initialize();
});
//# sourceMappingURL=main.js.map