(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "./src/RequestController"], factory);
    }
})(function (require, exports) {
    "use strict";
    var RequestController_1 = require("./src/RequestController");
    var requestController = new RequestController_1.RequestController();
    requestController.initialize();
});
//# sourceMappingURL=main_backend.js.map