var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "./AbstractRequestHandler", "../enum/RequestsEnum", "formidable", "fs", "../Config"], factory);
    }
})(function (require, exports) {
    "use strict";
    var AbstractRequestHandler_1 = require("./AbstractRequestHandler");
    var RequestsEnum_1 = require("../enum/RequestsEnum");
    var formidable_1 = require("formidable");
    var fs = require("fs");
    var Config_1 = require("../Config");
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    var DeleteBannerHandler = (function (_super) {
        __extends(DeleteBannerHandler, _super);
        function DeleteBannerHandler() {
            _super.apply(this, arguments);
        }
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        DeleteBannerHandler.prototype.init = function () {
            this.express.post(RequestsEnum_1.RequestsEnum.DELETE_BANNER, this.handler.bind(this));
        };
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        DeleteBannerHandler.prototype.deleteBanner = function () {
            var bannerName = this.fields["banner_name"];
            console.log("deleteBanner: " + bannerName);
            fs.unlinkSync(Config_1.Config.BANNERS_SAVE_PATH + bannerName);
            this.response.sendStatus(200);
        };
        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        DeleteBannerHandler.prototype.incomingFormParseCallbackHandler = function (err, fields, files) {
            this.fields = fields;
            this.deleteBanner();
        };
        DeleteBannerHandler.prototype.handler = function (req, res) {
            this.response = res;
            var form = new formidable_1.IncomingForm();
            form.parse(req, this.incomingFormParseCallbackHandler.bind(this));
        };
        return DeleteBannerHandler;
    }(AbstractRequestHandler_1.AbstractRequestHandler));
    exports.DeleteBannerHandler = DeleteBannerHandler;
});
//# sourceMappingURL=DeleteBannerHandler.js.map