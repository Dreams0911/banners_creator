var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "./AbstractRequestHandler", "../enum/RequestsEnum", "formidable", "fs", "../Config", "util"], factory);
    }
})(function (require, exports) {
    "use strict";
    var AbstractRequestHandler_1 = require("./AbstractRequestHandler");
    var RequestsEnum_1 = require("../enum/RequestsEnum");
    var formidable_1 = require("formidable");
    var fs = require("fs");
    var Config_1 = require("../Config");
    var util_1 = require("util");
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    var CreateBannerRequestHandler = (function (_super) {
        __extends(CreateBannerRequestHandler, _super);
        function CreateBannerRequestHandler() {
            _super.apply(this, arguments);
            this.NEW_CATHEGORY = "new";
            this.POPULAR_CATHEGORY = "popular";
            this.BANNER_NAME = "banner_name";
        }
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        CreateBannerRequestHandler.prototype.init = function () {
            this.generateOpenGameScript();
            this.express.post(RequestsEnum_1.RequestsEnum.CREATE_BANNER, this.handler.bind(this));
        };
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        CreateBannerRequestHandler.prototype.createBanner = function () {
            var bannerHTMLString = '';
            bannerHTMLString += '<html>';
            bannerHTMLString += '<head>';
            bannerHTMLString += '<link rel="stylesheet" type="text/css" href="common/style.css">';
            bannerHTMLString += this.openGameScriptString;
            bannerHTMLString += '</head>';
            bannerHTMLString += '<body>';
            bannerHTMLString += '   <table class="banners_block"><tr><td>';
            bannerHTMLString += this.createCategoryBlock(this.NEW_CATHEGORY);
            bannerHTMLString += '   <div class="banners_separator"> </div>';
            bannerHTMLString += this.createCategoryBlock(this.POPULAR_CATHEGORY);
            bannerHTMLString += '   </td></tr></table>';
            bannerHTMLString += '   <iframe id="pt_connection_frame" name="pt_connection_frame" frameborder="no"></iframe>';
            bannerHTMLString += '</body>';
            var bannerSavePath = Config_1.Config.BANNERS_SAVE_PATH + this.fields[this.BANNER_NAME] + ".html";
            fs.writeFile(bannerSavePath, bannerHTMLString, this.saveBannerHandler.bind(this));
        };
        // One block for NEW and one for POPULAR
        CreateBannerRequestHandler.prototype.createCategoryBlock = function (category) {
            var categoryBlock = '';
            categoryBlock += '<table class="banner">';
            categoryBlock += '   <tr><td> <img src="common/' + category + '.png" class="banner_title"/> </td></tr>';
            for (var i = 1; i <= 3; i++) {
                var shortName = this.fields[category + "_short_name_" + i];
                var fileKey = category + "_icon_" + i;
                // read binary data
                var bitmap = fs.readFileSync(this.files[fileKey].path);
                // convert binary data to base64 encoded string
                var base64Image = bitmap.toString('base64');
                var bannerImg = '<img src="data:' + this.files[fileKey].type + ';base64,' + base64Image + '\"/>';
                //==================================
                categoryBlock += '   <tr><td>';
                categoryBlock += '      <a href="#' + shortName + '" onclick="rungame(\'' + shortName + '\');">';
                categoryBlock += bannerImg;
                categoryBlock += '   </td></tr>';
            }
            categoryBlock += '   <tr class="banner_bottom"><td> </td></tr>';
            categoryBlock += '</table>';
            return categoryBlock;
        };
        // Script, which allow to open casino games in webclient and DL client
        CreateBannerRequestHandler.prototype.generateOpenGameScript = function () {
            this.openGameScriptString = '';
            this.openGameScriptString += '<script>';
            this.openGameScriptString += 'var params;';
            this.openGameScriptString += 'function getParams()';
            this.openGameScriptString += '{';
            this.openGameScriptString += '   var chunks = document.location.href.split(\'?\')[1].split(\'&\');';
            this.openGameScriptString += '   var chunks2;';
            this.openGameScriptString += '   var i;';
            this.openGameScriptString += '   var l;';
            this.openGameScriptString += '   l = chunks.length;';
            this.openGameScriptString += '   params = new Object();';
            this.openGameScriptString += '   for (i=0; i<l; i++)';
            this.openGameScriptString += '   {';
            this.openGameScriptString += '       chunks2 = chunks[i].split(\'=\'); params[chunks2[0]] = unescape(chunks2[1]);';
            this.openGameScriptString += '   }';
            this.openGameScriptString += '}';
            this.openGameScriptString += 'function rungame(game)';
            this.openGameScriptString += '{';
            this.openGameScriptString += '   if (params == null)';
            this.openGameScriptString += '   {';
            this.openGameScriptString += '       getParams();';
            this.openGameScriptString += '   }';
            this.openGameScriptString += '   var cmd;';
            this.openGameScriptString += '   cmd = params.pt_connection+\'?\'+new Date().getTime()+\'#\'+escape(\'switchtogame?code=\'+game);';
            this.openGameScriptString += '   window.frames[\'pt_connection_frame\'].location = cmd;';
            this.openGameScriptString += '}';
            this.openGameScriptString += '</script>';
        };
        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        CreateBannerRequestHandler.prototype.saveBannerHandler = function (err) {
            if (util_1.isNullOrUndefined(err)) {
                console.log(this.fields[this.BANNER_NAME] + " saved successfully");
                this.response.sendStatus(200);
            }
            else
                console.error(err);
        };
        CreateBannerRequestHandler.prototype.incomingFormParseCallbackHandler = function (err, fields, files) {
            this.fields = fields;
            this.files = files;
            this.createBanner();
        };
        CreateBannerRequestHandler.prototype.handler = function (req, res) {
            this.response = res;
            var form = new formidable_1.IncomingForm();
            form.parse(req, this.incomingFormParseCallbackHandler.bind(this));
        };
        return CreateBannerRequestHandler;
    }(AbstractRequestHandler_1.AbstractRequestHandler));
    exports.CreateBannerRequestHandler = CreateBannerRequestHandler;
});
//# sourceMappingURL=CreateBannerRequestHandler.js.map