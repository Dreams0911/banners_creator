import {AbstractRequestHandler} from "./AbstractRequestHandler";
import {RequestsEnum} from "../enum/RequestsEnum";
import {IncomingForm, File, Fields, Files} from "formidable";
import * as express from "express";
import * as fs from "fs";
import {Config} from "../Config";
import {isNullOrUndefined} from "util";

/**
 * Created by DreamsUltra10 on 04.12.2016.
 */

export class CreateBannerRequestHandler extends AbstractRequestHandler
{
    private NEW_CATHEGORY:string = "new";
    private POPULAR_CATHEGORY:string = "popular";
    private BANNER_NAME:string = "banner_name";

    private response:express.Response;

    private fields:Fields;
    private files:Files;

    private openGameScriptString:string;

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    protected init():void
    {
        this.generateOpenGameScript();
        this.express.post(RequestsEnum.CREATE_BANNER, this.handler.bind(this));
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    private createBanner():void
    {
        let bannerHTMLString:string = '';
        bannerHTMLString += '<html>';
        bannerHTMLString += '<head>';
        bannerHTMLString += '<link rel="stylesheet" type="text/css" href="common/style.css">';
        bannerHTMLString += this.openGameScriptString;
        bannerHTMLString += '</head>';

        bannerHTMLString += '<body>';
        bannerHTMLString += '   <table class="banners_block"><tr><td>';

        bannerHTMLString += this.createCategoryBlock(this.NEW_CATHEGORY);
        bannerHTMLString += '   <div class="banners_separator"> </div>';
        bannerHTMLString += this.createCategoryBlock(this.POPULAR_CATHEGORY);

        bannerHTMLString += '   </td></tr></table>';
        bannerHTMLString += '   <iframe id="pt_connection_frame" name="pt_connection_frame" frameborder="no"></iframe>';
        bannerHTMLString += '</body>';

        let bannerSavePath:string = Config.BANNERS_SAVE_PATH + this.fields[this.BANNER_NAME] + ".html";
        fs.writeFile(bannerSavePath, bannerHTMLString, this.saveBannerHandler.bind(this));
    }

    // One block for NEW and one for POPULAR
    private createCategoryBlock(category:string):string
    {
        let categoryBlock:string = '';
        categoryBlock += '<table class="banner">';
        categoryBlock += '   <tr><td> <img src="common/' + category + '.png" class="banner_title"/> </td></tr>';

        for(let i:number=1; i<=3; i++)
        {
            let shortName:string = this.fields[category + "_short_name_"+i];
            let fileKey:string = category + "_icon_" + i;

            // read binary data
            let bitmap:Buffer = fs.readFileSync(this.files[fileKey].path);
            // convert binary data to base64 encoded string
            let base64Image:any = bitmap.toString('base64');
            let bannerImg:string = '<img src="data:' + this.files[fileKey].type + ';base64,' + base64Image + '\"/>';
            //==================================
            categoryBlock += '   <tr><td>';
            categoryBlock += '      <a href="#'+shortName+'" onclick="rungame(\''+shortName+'\');">';
            categoryBlock += bannerImg;
            categoryBlock += '   </td></tr>';
        }
        categoryBlock += '   <tr class="banner_bottom"><td> </td></tr>';
        categoryBlock += '</table>';

        return categoryBlock;
    }

    // Script, which allow to open casino games in webclient and DL client
    private generateOpenGameScript():void
    {
        this.openGameScriptString = '';
        this.openGameScriptString += '<script>';
        this.openGameScriptString += 'var params;';
        this.openGameScriptString += 'function getParams()';
        this.openGameScriptString += '{';
        this.openGameScriptString += '   var chunks = document.location.href.split(\'?\')[1].split(\'&\');';
        this.openGameScriptString += '   var chunks2;';
        this.openGameScriptString += '   var i;';
        this.openGameScriptString += '   var l;';
        this.openGameScriptString += '   l = chunks.length;';
        this.openGameScriptString += '   params = new Object();';
        this.openGameScriptString += '   for (i=0; i<l; i++)';
        this.openGameScriptString += '   {';
        this.openGameScriptString += '       chunks2 = chunks[i].split(\'=\'); params[chunks2[0]] = unescape(chunks2[1]);';
        this.openGameScriptString += '   }';
        this.openGameScriptString += '}';

        this.openGameScriptString += 'function rungame(game)';
        this.openGameScriptString += '{';
        this.openGameScriptString += '   if (params == null)';
        this.openGameScriptString += '   {';
        this.openGameScriptString += '       getParams();';
        this.openGameScriptString += '   }';
        this.openGameScriptString += '   var cmd;';
        this.openGameScriptString += '   cmd = params.pt_connection+\'?\'+new Date().getTime()+\'#\'+escape(\'switchtogame?code=\'+game);';
        this.openGameScriptString += '   window.frames[\'pt_connection_frame\'].location = cmd;';
        this.openGameScriptString += '}';
        this.openGameScriptString += '</script>';
    }

    //--------------------------------------------------------------------------
    //								HANDLERS
    //--------------------------------------------------------------------------
    private saveBannerHandler(err:NodeJS.ErrnoException):void
    {
        if(isNullOrUndefined(err))
        {
            console.log(this.fields[this.BANNER_NAME] + " saved successfully");
            this.response.sendStatus(200);
        }
        else
            console.error(err);
    }

    private incomingFormParseCallbackHandler(err:any, fields:Fields, files:Files):void
    {
        this.fields = fields;
        this.files = files;

        this.createBanner();
    }

    private handler(req:express.Request, res:express.Response):void
    {
        this.response = res;

        let form:IncomingForm = new IncomingForm();
        form.parse(req, this.incomingFormParseCallbackHandler.bind(this));
    }
}