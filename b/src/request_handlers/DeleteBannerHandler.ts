import {AbstractRequestHandler} from "./AbstractRequestHandler";
import {RequestsEnum} from "../enum/RequestsEnum";
import {IncomingForm, File, Fields, Files} from "formidable";
import * as express from "express";
import * as fs from "fs";
import {Config} from "../Config";
import {isNullOrUndefined} from "util";

/**
 * Created by DreamsUltra10 on 04.12.2016.
 */

export class DeleteBannerHandler extends AbstractRequestHandler
{
    private response:express.Response;

    private fields:Fields;

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    protected init():void
    {
        this.express.post(RequestsEnum.DELETE_BANNER, this.handler.bind(this));
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    private deleteBanner():void
    {
        let bannerName:string = this.fields["banner_name"];
        console.log("deleteBanner: " + bannerName);

        fs.unlinkSync(Config.BANNERS_SAVE_PATH + bannerName)
        this.response.sendStatus(200);
    }

    //--------------------------------------------------------------------------
    //								HANDLERS
    //--------------------------------------------------------------------------
    private incomingFormParseCallbackHandler(err:any, fields:Fields, files:Files):void
    {
        this.fields = fields;
        this.deleteBanner();
    }

    private handler(req:express.Request, res:express.Response):void
    {
        this.response = res;

        let form:IncomingForm = new IncomingForm();
        form.parse(req, this.incomingFormParseCallbackHandler.bind(this));
    }
}