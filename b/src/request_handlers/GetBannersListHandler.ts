import {AbstractRequestHandler} from "./AbstractRequestHandler";
import {RequestsEnum} from "../enum/RequestsEnum";
import {IncomingForm, File, Fields, Files} from "formidable";
import * as express from "express";
import * as fs from "fs";
import {Config} from "../Config";
import {isNullOrUndefined} from "util";

/**
 * Created by DreamsUltra10 on 04.12.2016.
 */

export class GetBannersListHandler extends AbstractRequestHandler
{
    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    protected init():void
    {
        this.express.post(RequestsEnum.GET_BANNERS_LIST, this.handler.bind(this));
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    //								HANDLERS
    //--------------------------------------------------------------------------

    private handler(req:express.Request, res:express.Response):void
    {
        let dirItems:string[] = fs.readdirSync(Config.BANNERS_SAVE_PATH);
        let bannersList:Array<string> = [];
        for(let i:number=0; i<dirItems.length; i++)
        {
            // Checking required, because banners directory contains common assets.
            if(dirItems[i].indexOf(".html") != -1)
            {
                bannersList.push(dirItems[i]);
            }
        }

        console.log(bannersList);
        res.send(bannersList);
    }
}