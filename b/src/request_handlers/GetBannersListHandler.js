var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "./AbstractRequestHandler", "../enum/RequestsEnum", "fs", "../Config"], factory);
    }
})(function (require, exports) {
    "use strict";
    var AbstractRequestHandler_1 = require("./AbstractRequestHandler");
    var RequestsEnum_1 = require("../enum/RequestsEnum");
    var fs = require("fs");
    var Config_1 = require("../Config");
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    var GetBannersListHandler = (function (_super) {
        __extends(GetBannersListHandler, _super);
        function GetBannersListHandler() {
            _super.apply(this, arguments);
        }
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        GetBannersListHandler.prototype.init = function () {
            this.express.post(RequestsEnum_1.RequestsEnum.GET_BANNERS_LIST, this.handler.bind(this));
        };
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------
        //								HANDLERS
        //--------------------------------------------------------------------------
        GetBannersListHandler.prototype.handler = function (req, res) {
            var dirItems = fs.readdirSync(Config_1.Config.BANNERS_SAVE_PATH);
            var bannersList = [];
            for (var i = 0; i < dirItems.length; i++) {
                // Checking required, because banners directory contains common assets.
                if (dirItems[i].indexOf(".html") != -1) {
                    bannersList.push(dirItems[i]);
                }
            }
            console.log(bannersList);
            res.send(bannersList);
        };
        return GetBannersListHandler;
    }(AbstractRequestHandler_1.AbstractRequestHandler));
    exports.GetBannersListHandler = GetBannersListHandler;
});
//# sourceMappingURL=GetBannersListHandler.js.map