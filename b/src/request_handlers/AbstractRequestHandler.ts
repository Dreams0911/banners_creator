import * as express from "express";
/**
 * Created by DreamsUltra10 on 04.12.2016.
 */
export abstract class AbstractRequestHandler
{
    constructor(protected express:express.Express)
    {
        this.init();
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    protected abstract init():void;
}