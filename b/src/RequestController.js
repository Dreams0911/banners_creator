(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports", "express", "./Config", "./request_handlers/CreateBannerRequestHandler", "./request_handlers/GetBannersListHandler", "./request_handlers/DeleteBannerHandler"], factory);
    }
})(function (require, exports) {
    "use strict";
    var express = require("express");
    var Config_1 = require("./Config");
    var CreateBannerRequestHandler_1 = require("./request_handlers/CreateBannerRequestHandler");
    var GetBannersListHandler_1 = require("./request_handlers/GetBannersListHandler");
    var DeleteBannerHandler_1 = require("./request_handlers/DeleteBannerHandler");
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    var RequestController = (function () {
        function RequestController() {
        }
        //--------------------------------------------------------------------------
        //							PUBLIC METHODS
        //--------------------------------------------------------------------------
        RequestController.prototype.initialize = function () {
            this.express = express();
            //noinspection TypeScriptValidateTypes
            this.express.use(this.allowAccess.bind(this));
            this.initHandlers();
            // Listen
            this.express.listen(Config_1.Config.SERVER_PORT);
            console.log('Server running on port:' + Config_1.Config.SERVER_PORT);
        };
        //--------------------------------------------------------------------------
        //					  PRIVATE\PROTECTED METHODS
        //--------------------------------------------------------------------------
        RequestController.prototype.initHandlers = function () {
            this.handlers = [];
            this.handlers.push(new CreateBannerRequestHandler_1.CreateBannerRequestHandler(this.express));
            this.handlers.push(new GetBannersListHandler_1.GetBannersListHandler(this.express));
            this.handlers.push(new DeleteBannerHandler_1.DeleteBannerHandler(this.express));
        };
        RequestController.prototype.allowAccess = function (req, res, next) {
            // Website you wish to allow to connect
            res.setHeader('Access-Control-Allow-Origin', '*');
            // Request methods you wish to allow
            //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Methods', 'POST');
            // Request headers you wish to allow
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
            // Pass to next layer of middleware
            next();
        };
        return RequestController;
    }());
    exports.RequestController = RequestController;
});
//# sourceMappingURL=RequestController.js.map