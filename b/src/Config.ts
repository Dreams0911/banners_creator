/**
 * Created by DreamsUltra10 on 04.12.2016.
 */
export class Config
{
    static readonly SERVER_PORT:number = 8080;
    static readonly BANNERS_SAVE_PATH:string = "./banners/";
}