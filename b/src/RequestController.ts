import * as express from "express";
import {Response, NextFunction, Request, IRouterHandler, RequestHandler} from "express-serve-static-core";
import {Config} from "./Config";
import {AbstractRequestHandler} from "./request_handlers/AbstractRequestHandler";
import {CreateBannerRequestHandler} from "./request_handlers/CreateBannerRequestHandler";
import {GetBannersListHandler} from "./request_handlers/GetBannersListHandler";
import {DeleteBannerHandler} from "./request_handlers/DeleteBannerHandler";
/**
 * Created by DreamsUltra10 on 04.12.2016.
 */

export class RequestController
{
    private express:express.Express;
    private handlers:Array<AbstractRequestHandler>;

    constructor()
    {
    }

    //--------------------------------------------------------------------------
    //							PUBLIC METHODS
    //--------------------------------------------------------------------------
    initialize():void
    {
        this.express = express();
        //noinspection TypeScriptValidateTypes
        this.express.use(this.allowAccess.bind(this));
        this.initHandlers();

        // Listen
        this.express.listen(Config.SERVER_PORT);
        console.log('Server running on port:' + Config.SERVER_PORT);
    }

    //--------------------------------------------------------------------------
    //					  PRIVATE\PROTECTED METHODS
    //--------------------------------------------------------------------------
    private initHandlers():void
    {
        this.handlers = [];
        this.handlers.push(new CreateBannerRequestHandler(this.express));
        this.handlers.push(new GetBannersListHandler(this.express));
        this.handlers.push(new DeleteBannerHandler(this.express));
    }

    private allowAccess(req:Request, res:Response, next:NextFunction):void
    {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');

        // Request methods you wish to allow
        //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Methods', 'POST');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        // Pass to next layer of middleware
        next();
    }
}