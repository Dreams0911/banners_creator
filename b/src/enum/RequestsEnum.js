(function (factory) {
    if (typeof module === 'object' && typeof module.exports === 'object') {
        var v = factory(require, exports); if (v !== undefined) module.exports = v;
    }
    else if (typeof define === 'function' && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    /**
     * Created by DreamsUltra10 on 04.12.2016.
     */
    // TODO: Remove duplicate class, from backend
    var RequestsEnum = (function () {
        function RequestsEnum() {
        }
        RequestsEnum.CREATE_BANNER = "/create_banner";
        RequestsEnum.GET_BANNERS_LIST = "/get_banners_list";
        RequestsEnum.DELETE_BANNER = "/delete_banner";
        return RequestsEnum;
    }());
    exports.RequestsEnum = RequestsEnum;
});
//# sourceMappingURL=RequestsEnum.js.map